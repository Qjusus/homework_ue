// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "HOMEWORK_UEGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class HOMEWORK_UE_API AHOMEWORK_UEGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
